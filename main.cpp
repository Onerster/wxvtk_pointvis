
#include <cstdlib>
#include <sstream>
#include <vector>
#include <algorithm>


#include <vtk-7.1/vtkAutoInit.h>

VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
//#define vtkRenderingCore_AUTOINIT 2(vtkRenderingOpenGL2, vtkInteractionStyle)
//#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL2,vtkRenderingOpenGL2)
//#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)

#include <vtk-7.1/vtkNew.h>
#include <vtk-7.1/vtkSmartPointer.h>
#include <vtk-7.1/vtkConeSource.h>
#include <vtk-7.1/vtkPolyDataMapper.h>
#include <vtk-7.1/vtkRenderWindow.h>
#include <vtk-7.1/vtkRenderWindowInteractor.h>
#include <vtk-7.1/vtkCamera.h>
#include <vtk-7.1/vtkActor.h>
#include <vtk-7.1/vtkRenderer.h>
#include <vtk-7.1/vtkInteractorStyleTrackballCamera.h>
#include <vtk-7.1/vtkVertexGlyphFilter.h>
#include <vtk-7.1/vtkNamedColors.h>
#include <vtk-7.1/vtkProperty.h>
#include <vtk-7.1/vtkLookupTable.h>
#include <vtk-7.1/vtkDataArray.h>
#include <vtk-7.1/vtkFloatArray.h>
#include <vtk-7.1/vtkPointData.h>
#include <vtk-7.1/vtkRenderWindowInteractor.h>
#include <vtk-7.1/vtkPoints.h>
#include <vtk-7.1/vtkPointWidget.h>
#include <vtk-7.1/vtkCellArray.h>
#include <vtk-7.1/vtkPolyData.h>
#include <vtk-7.1/vtkLabeledDataMapper.h>

#include "TableReader/TableReader.hpp"

std::vector<LineDataOfFile> GenerateTestPointData()
{
  std::vector<LineDataOfFile> dataVec;
  for (int i = 0; i < 20; i++)
  {
    for (int j = 0; j < 20; j++)
    {
      double x = i * 0.2;
      double y = j * 0.1;
      double z = 0.1 * x * x + 0.1 * y * y;
      
      LineDataOfFile currentData;
      currentData.vals.push_back(x);
      currentData.vals.push_back(y);
      currentData.vals.push_back(z);
      
      dataVec.push_back(currentData);
    }
  }
  
  return dataVec;
}

void VisualizeSetOfPoints()
{
  
  vtkNew<vtkNamedColors> colors;
  
  /*std::vector<LineDataOfFile> dataVec = 
        GenerateTestPointData();*/
  std::vector<LineDataOfFile> dataVec = 
          GetFileData( wxT("points_irregularxydistance.csv"));
  
  unsigned long numOfPoints = dataVec.size();
  
  //creating the point data structure
  vtkSmartPointer<vtkPointData> pointData = 
          vtkSmartPointer<vtkPointData>::New();  
  
  vtkSmartPointer<vtkFloatArray> floatArray = 
          vtkSmartPointer<vtkFloatArray>::New();
  
  //setting up the points
  vtkSmartPointer<vtkPoints> points = 
          vtkSmartPointer<vtkPoints>::New();
  
  std::vector<double> zVec;
  
  for (long i = 0; i < numOfPoints; i++)
  {
    LineDataOfFile line = dataVec[i];
    double x = line.vals[0];
    double y = line.vals[1];
    double z = line.vals[2];
    
    zVec.push_back( z);
    
    points->InsertNextPoint(x, y, z);
    
    floatArray->InsertNextValue(z);
  }
  
  //calculating the max min scalar values
  
  double zmin = *std::min_element( std::begin( zVec),
                                   std::end( zVec));
  double zmax = *std::max_element( std::begin( zVec),
                                   std::end( zVec));
  
  //generating the lookup table
  //vtkSmartPointer<vtkLookupTable> lookupTable = 
          //vtkSmartPointer<vtkLookupTable>::New();
  //vtkSmartPointer<vtkUnsignedCharArray> lookupArray = 
          //lookupTable->MapScalars(floatArray, VTK_COLOR_MODE_DEFAULT, -1);
  
  //generating the polydata  
  vtkSmartPointer<vtkPolyData> polyData = 
          vtkSmartPointer<vtkPolyData>::New();
  polyData->SetPoints( points);
  polyData->GetPointData()->SetScalars( floatArray);
  //polyData->GetPointData()->Set
    

  
  vtkSmartPointer<vtkPolyDataMapper> mapper = 
          vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputData( polyData);
  mapper->SetScalarModeToUsePointData();
  mapper->SetScalarRange( zmin, zmax);
   
  vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter = 
          vtkSmartPointer<vtkVertexGlyphFilter>::New();
  mapper->SetInputConnection( glyphFilter->GetOutputPort());
  glyphFilter->SetInputData( polyData);
  glyphFilter->Update();
  
  vtkSmartPointer<vtkActor> actor = 
          vtkSmartPointer<vtkActor>::New();
  actor->SetMapper( mapper);
  //actor->GetProperty()->SetColor( colors->GetColor3d("Red").GetData());
  //actor->GetProperty()->SetPointSize(2);
    
  vtkSmartPointer<vtkRenderer> rend = 
          vtkSmartPointer<vtkRenderer>::New();
  rend->AddActor( actor);
  rend->SetBackground(0.8, 0.8, 0.8);
  
  vtkSmartPointer<vtkRenderWindow> renWin = 
          vtkSmartPointer<vtkRenderWindow>::New();
  renWin->AddRenderer( rend);
  renWin->SetSize( 800, 800);
  
  vtkSmartPointer<vtkRenderWindowInteractor> iren = 
          vtkSmartPointer<vtkRenderWindowInteractor>::New();
  iren->SetRenderWindow( renWin);
  
  vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = 
          vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
  iren->SetInteractorStyle( style);
  
  iren->Initialize();
  iren->Start();
  
  polyData->Delete();
  mapper->Delete();
  actor->Delete();
  rend->Delete();
  renWin->Delete();
  iren->Delete();
  style->Delete();
}

int main(int argc, char** argv)
{
   
  VisualizeSetOfPoints();
  
  return 0;
}

