#include "TableReader.hpp"

std::vector<LineDataOfFile> GetFileData(wxString fileName)
{
  std::vector<LineDataOfFile> fileData;  
  wxTextFile fileHandler(fileName);
  fileHandler.Open();
  int numOfLines = fileHandler.GetLineCount();
  for (int lineNumber = 0; lineNumber < numOfLines; lineNumber++)
  {
    LineDataOfFile currentData;

    wxString currentLine = fileHandler.GetLine( lineNumber);
    wxStringTokenizer tkz(currentLine, wxT(","));
    wxString token = tkz.GetNextToken();
    do
    {
      double currentValue;
      token.ToDouble(&currentValue);
      
      currentData.vals.push_back(currentValue);
      
      token = tkz.GetNextToken();
    } while(tkz.HasMoreTokens());
    double currentValue;
    token.ToDouble(&currentValue);
      
    currentData.vals.push_back(currentValue);
    
    fileData.push_back(currentData);
  }
  fileHandler.Close();
  return fileData;
}