#ifndef TABLEREADER_HPP_INCLUDED
#define TABLEREADER_HPP_INCLUDED

#include <iostream>
#include <vector>
#include <wx/wx.h>
#include <wx/textfile.h>
#include <wx/string.h>
#include <wx/tokenzr.h>

struct LineDataOfFile
{
  std::vector<double> vals;
};

std::vector<LineDataOfFile> GetFileData(wxString fileName);


#endif // TABLEREADER_HPP_INCLUDED
